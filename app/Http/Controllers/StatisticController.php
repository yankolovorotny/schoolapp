<?php

namespace App\Http\Controllers;

use App\Classes;
use App\ClassRoom;
use App\Desert;
use App\Diamond;
use App\Journal;
use App\Lesson;
use App\Schedule;
use App\SchoolClass;
use App\Student;
use App\Teaching;
use App\Time;
use App\Worker;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'journals' => Journal::getByStudents(),
            'schoolclasses' => SchoolClass::all()
        ];
        foreach ($data['journals'] as $journal){
            $class = Classes::find($journal->class);
            $journal->classes = $journal->student = '';
            if(!empty($journal->studentsID)){
                $student = Student::find($journal->studentsID);
                $prop = 'class';
                $schoolclass = SchoolClass::find($student->$prop);
                $journal->student =  $student->surname.' '.$student->name.', '.$schoolclass->name;
            }
            $journal->class_rooms = $journal->school_classes =  $journal->teachings = $journal->times = '';
            if(!empty($class)) {
                $schedule = Schedule::find($class->schedulesID);
                if(!empty($schedule)){

                    $times = Time::find($schedule->timesID);
                    $teachings = Teaching::find($schedule->teachingsID);
                    $school_classes = SchoolClass::find($schedule->school_classesID);
                    $class_rooms = ClassRoom::find($schedule->class_roomsID);
                    if(!empty($times))
                        $journal->times =  date("H:i",strtotime($times->startLess)).' - '.date("H:i",strtotime($times->endLess));
                    //dd($class->times);
                    if(!empty($teachings)) {
                        $lesson_name = Lesson::find($teachings->lesson);
                        $worker_name = Worker::find($teachings->teacher);
                        $journal->teachings = $lesson_name->name . ' - ' . $worker_name->surname . ' '. $worker_name->name . ' '. $worker_name->patronymic;
                        //dd($worker_name);
                    }
                    if(!empty($school_classes))
                        $journal->school_classes = $school_classes->name;

                }
                //$journal->classes = $class->class_rooms.' '.$class->teachings;
            }
        }
        //dd($data['journals']);
        return view('statistic/index',$data);
    }
    public function getByDiamonds()
    {
        $data = [
            'deserts' => Desert::all(),
            'diamonds' => Diamond::all(),
            'students' => Student::all(),
            'schoolclasses' => SchoolClass::all()
        ];
        foreach ($data['deserts'] as $desert){
            $desert->classes = $desert->student = $desert->worker = '';
            if(!empty($desert->teacher)){
                $worker = Worker::find($desert->teacher);
                $desert->worker =  $worker->surname.' '.$worker->name.' '.$worker->patronymic;
            }
        }
        //dd($data['students']);
        foreach ($data['students'] as $student){
            $deserts = [];
            $prop = 'class';
            $schoolclass = SchoolClass::find($student->$prop);
            if(!empty($schoolclass))
                $student->schoolclass = $schoolclass->id;
            $student->student =  $student->surname.' '.$student->name.', '.$schoolclass->name;
            foreach ($data['deserts'] as $desert){
                if($desert->studentsID == $student->id){
                    $deserts[] = $desert;
                }
            }
            $student->deserts = $deserts;
        }
        //dd($data['students']);
        return view('statistic/diamond',$data);
    }
}
