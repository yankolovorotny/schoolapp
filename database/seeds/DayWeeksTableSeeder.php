<?php

use Illuminate\Database\Seeder;

class DayWeeksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $days = ["Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"];
        foreach ($days as $day){
            DB::table('day_weeks')->insert([
                'name' => $day
            ]);
        }
    }
}
