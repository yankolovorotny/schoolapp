<?php

namespace App\Http\Controllers;

use App\Parenting;
use App\SchoolClass;
use App\Student;
use App\StudentParent;
use App\UserRole;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'schoolclasses' => SchoolClass::all(),
            'students' =>  Student::all()
        ];
        foreach ($data['students'] as $student){
            $student->email = '';
            if(User::find($student->id)){
                $user = User::find($student->id);
                $student->email = $user->email;
            }
            $parentings =  Parenting::where('studentID','=',$student->id)->take(2)->get();
            $childrens = [];
            foreach ($parentings as $parenting){
                $childrens[$parenting->id] = StudentParent::find($parenting->parentID);
            }
            $student->parents = $childrens;
            //dd($student->parents);
        }
          //dd($data['students']);
        return view('students/main/'.\App\UserRole::getRole(),$data);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $students = Student::all();
        foreach ($students as $student){
            $prop = 'class';
            $schoolclass = SchoolClass::find($student->$prop);
            $data[] = ["value" => $student->id, "text" => $student->surname.' '.$student->name.', '.$schoolclass->name];
        }
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        \App\User::create([
            'name'=>'Ученик',
            'role'=>UserRole::where('name','=','student')->value('id'),
            'password'=>Hash::make('123qweasd'),
            'email'=>'s'.time().'@example.com'
        ]);
        $lastInsertId = \App\User::select('id')->orderBy('id', 'DESC')->first();
        Student::create([
            'id'=>$lastInsertId->id,
            'name'=>'Имя',
            'surname'=>'Фамилия',
            'class'=>$id,
            'sex'=>1
        ]);

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('pk');
        $name = $request->input('name');
        $value = $request->input('value');
        $result = Student::where('id', '=', $id)->update([$name => $value]);
        $data = [
            'response' => $result
        ];
        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::find($id)->delete();
        return redirect()->back();
    }
}
