@if ( !empty($deserts) )
    <tr>
        <th>Ученик</th>
        <th>Диамант</th>
        <th>Выдал(а)</th>
        <th>Примечание</th>
        <th></th>
    </tr>

    @foreach( $deserts as $desert )

        <tr>
            <td><a class="edit-studentsID" data-pk="{{ $desert->id }}" data-name="studentsID" data-type="select" data-source="{{ route('students/getList') }}">{{ $desert->student }}</a></td>
            {{--<td><a class="edit-desertsID" data-pk="{{ $desert->id }}" data-name="desertsID" >123 </a></td>--}}
            <td><div class="diamonds" id="diamonds{{ $desert->id }}"  data-pk="{{ $desert->id }}" data-val="{{ $desert->diamondsID }}"></div></td>
            <td><a class="edit-teacher" data-pk="{{ $desert->id }}" data-name="teacher" data-type="select" data-source="{{ route('workers/getList') }}">{{ $desert->worker }}</a></td>
            <td><a class="edit-reason" data-pk="{{ $desert->id }}" data-name="reason" >{{ $desert->reason }}</a></td>
            <td><a href="{{ route('deserts/delete',['id' => $desert->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
    @endforeach
@endif
<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };

        $.each($('.diamonds'), function (index,value) {
            var id = $(value).data('pk');
            var val = $(value).data('val');
            var iconSelect;
            var selectedText;

            //selectedText = document.getElementById('selected-text');

            iconSelect = new IconSelect("diamonds"+id);
            var icons = [];
            icons.push({'iconFilePath':'/images/diamond_red.svg', 'iconValue':'1', 'description':'Знания: учеба'});
            icons.push({'iconFilePath':'/images/diamond_orange.svg', 'iconValue':'2', 'description':'Лидерство: поддержка, старейшинство'});
            icons.push({'iconFilePath':'/images/diamond_yellow.svg', 'iconValue':'3', 'description':'Спорт'});
            icons.push({'iconFilePath':'/images/diamond_green.svg', 'iconValue':'4', 'description':'Искусство'});
            icons.push({'iconFilePath':'/images/diamond_blue.svg', 'iconValue':'5', 'description':'Труд'});
            icons.push({'iconFilePath':'/images/diamond_purple.svg', 'iconValue':'6', 'description':'Игры'});

            iconSelect.refresh(icons);
            iconSelect.setSelectedIndex(val-1);
            iconSelect.setTitle(val-1);

            document.getElementById("diamonds"+id).addEventListener('changed', function(e){
                $.ajax({
                    type: 'PUT',
                    url: '<?php echo e(route('deserts/update')); ?>',
                    data: {
                        _token: _token,
                        pk: id,
                        name: 'diamondsID',
                        value: iconSelect.getSelectedValue()
                    },
                    success: function (response) {
                        iconSelect.setTitle(iconSelect.getSelectedValue()-1);
                    }
                });
            });
        });





        $('.edit-studentsID,.edit-desertsID,.edit-reason,.edit-teacher').editable({
            url: '{{ route('deserts/update') }}',
            title: 'Выберите значение'
        });
        $('#combodate').editable({
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 2000,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('#combodate').on('hidden', function(e, params) {
            var dates = $('#combodate').text();
            var arr = dates.split('.');
            //alert("/deserts/"+arr[2]+'-'+arr[1]+'-'+arr[0]);
            if(arr[0] == '' || arr[0] == '' || arr[0] == '')
                window.location = "/deserts/";
            else
                window.location = "/deserts/"+arr[2]+'-'+arr[1]+'-'+arr[0];
        });
    }
</script>
