<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desert extends Model
{
    protected $fillable = ['date'];

    public static function getByDate($date)
    {
        return collect(\DB::select(\DB::raw('SELECT * FROM deserts WHERE DATE(date) = "'.$date.'" ORDER BY diamondsID')));
    }
}
