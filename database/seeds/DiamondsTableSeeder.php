<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiamondsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('diamonds')->insert([
            'color' => 'Красный',
            'active' => 1,
            'src' => '/images/diamond_red.svg',
            'description' => 'Знания: учеба'
        ]);
        DB::table('diamonds')->insert([
            'color' => 'Оранжевый',
            'active' => 1,
            'src' => '/images/diamond_orange.svg',
            'description' => 'Лидерство: поддержка, старейшинство'
        ]);
        DB::table('diamonds')->insert([
            'color' => 'Желтый',
            'active' => 1,
            'src' => '/images/diamond_yellow.svg',
            'description' => 'Спорт'
        ]);
        DB::table('diamonds')->insert([
            'color' => 'Зеленый',
            'active' => 1,
            'src' => '/images/diamond_green.svg',
            'description' => 'Искусство'
        ]);
        DB::table('diamonds')->insert([
            'color' => 'Синий',
            'active' => 1,
            'src' => '/images/diamond_blue.svg',
            'description' => 'Труд'
        ]);
        DB::table('diamonds')->insert([
            'color' => 'Фиолетовый',
            'active' => 1,
            'src' => '/images/diamond_purple.svg',
            'description' => 'Игры'
        ]);
    }
}
