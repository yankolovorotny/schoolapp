<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentingsTable extends Migration
{
    /**
     * Run the migrations. Родители ребенка
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parentings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentID');
            $table->integer('parentID');
            $table->unique(['studentID', 'parentID']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parentings');
    }
}
