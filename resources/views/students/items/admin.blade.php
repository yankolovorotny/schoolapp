@if ( !empty($schoolclasses) )
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    @foreach( $schoolclasses as $schoolclass )


            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{$schoolclass->id}}">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$schoolclass->id}}" aria-expanded="true" aria-controls="collapse{{$schoolclass->id}}">
                        Учащиеся {{$schoolclass->name}} класса
                    </a>
                    <a  role="button"   href="{{ route('students/create', ['id' => $schoolclass->id]) }}" class="btn btn-xs btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Добавить учащегося</a>
                </div>
                <div id="collapse{{$schoolclass->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$schoolclass->id}}">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>Фамилия</th>
                                    <th>Имя</th>
                                    <th>Отчество</th>
                                    <th>Пол</th>
                                    <th>Дата рождения</th>
                                    <th>Адрес</th>
                                    <th>Телефон</th>
                                    <th>Примечание</th>
                                    <th></th>
                                </tr>
                                @foreach( $students as $student )
                                    @if( $student->class == $schoolclass->id)
                                        <tr>
                                            <td>
                                                <a class="edit-surname" data-pk="{{ $student->id }}" data-name="surname">{{ $student->surname }}</a>
                                            </td>
                                            <td>
                                                <a class="edit-name" data-pk="{{ $student->id }}" data-name="name">{{ $student->name }}</a>
                                            </td>
                                            <td>
                                                <a class="edit-patronymic" data-pk="{{ $student->id }}" data-name="patronymic">{{ $student->patronymic }}</a>
                                            </td>
                                            <td>
                                                <a class="edit-sex" data-pk="{{ $student->id }}" data-name="sex" data-type="select">{{ (($student->sex == 1)?'М':'Ж') }}</a>
                                            </td>
                                            <td>
                                                <a class="edit-birthday" data-pk="{{ $student->id }}" data-name="birthday" data-type="combodate">{{ date("d.m.Y",strtotime($student->birthday)) }}</a>
                                            </td>
                                            <td>
                                                <a class="edit-location" data-pk="{{ $student->id }}" data-name="location">{{ $student->location }}</a>
                                            </td>
                                            <td>
                                                <a class="edit-phone" data-pk="{{ $student->id }}" data-name="phone">{{ $student->phone }}</a>
                                            </td>
                                            <td>
                                                <a class="edit-remark" data-pk="{{ $student->id }}" data-name="remark">{{ $student->remark }}</a>
                                            </td>
                                            <td><a href="{{ route('students/delete',['id' => $student->id]) }}"
                                                   class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        </tr>
                                        <tr class="info">
                                            <th colspan="3"><span class="glyphicon glyphicon-user"></span>
                                                <a class="edit-username" data-pk="{{ $student->id }}" data-name="email">{{ $student->email }}</a>
                                                <button class="btn btn-xs btn-primary pull-right btn-reset-password" data-pk="{{ $student->id }}">Сброс пароля</button>
                                            </th>
                                            <td colspan="6">Родители:
                                                @foreach($student->parents as $parent)
                                                     {{$parent->surname}} {{$parent->name}} {{$parent->patronymic}}
                                                    <a href="callto:{{$parent->phone}}" title="{{$parent->phone}}" class="btn btn-xs btn-primary" style="line-height: 1;"><span class="glyphicon glyphicon-phone"></span></a>&nbsp;
                                                @endforeach
                                            </td>

                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>

    @endforeach
    </div>
@endif

<script>
    window.onload = function () {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-name,.edit-surname,.edit-patronymic,.edit-location,.edit-phone,.edit-remark').editable({
            type: 'text',
            url: '{{ route('students/update') }}',
            title: 'Введите значение'
        });
        $('.edit-username').editable({
            type: 'text',
            url: '{{ route('users/update') }}',
            title: 'Введите значение'
        });
        $('.edit-sex').editable({
            source: [
                {value: 1, text: 'М'},
                {value: 2, text: 'Ж'}
            ],
            url: '{{ route('students/update') }}',
            title: 'Укажите пол'
        });
        $('.edit-birthday').editable({
            url: '{{ route('students/update') }}',
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 2000,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('body').on('click','.btn-reset-password',function () {
            var pk = $(this).data('pk');
            swal({
                        title: "Пароль",
                        text: "Введите новый пароль",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        animation: "slide-from-top",
                        inputPlaceholder: "Пароль"
                    },
                    function(inputValue){
                        if (inputValue === false) return false;

                        if (inputValue === "") {
                            swal.showInputError("Пароль не может быть пустым!");
                            return false
                        }

                        swal("Пароль изменен!", "Текущий пароль: " + inputValue, "success");

                        $.ajax({
                            type: 'PUT',
                            url: '{{ route('users/update') }}',
                            data: {
                                _token: _token,
                                pk: pk,
                                name: 'password',
                                value: inputValue
                            },
                            success: function (response) {
                            }
                        });

                    });
        })
    }
</script>