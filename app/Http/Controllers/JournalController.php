<?php

namespace App\Http\Controllers;

use App\Classes;
use App\ClassRoom;
use App\Journal;
use App\Lesson;
use App\Schedule;
use App\SchoolClass;
use App\Student;
use App\Teaching;
use App\Time;
use App\Worker;
use Illuminate\Http\Request;

class JournalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($date = null)
    {
        if(empty($date))
            $date = date("Y-m-d");
        $data = [
            'date' => $date,
            'journals' => Journal::getByDate($date)
        ];
        foreach ($data['journals'] as $journal){
            $class = Classes::find($journal->classesID);
            $journal->classes = $journal->student = '';
            if(!empty($journal->studentsID)){
                $student = Student::find($journal->studentsID);
                $prop = 'class';
                $schoolclass = SchoolClass::find($student->$prop);
                $journal->student =  $student->surname.' '.$student->name.', '.$schoolclass->name;
            }
            $journal->class_rooms = $journal->school_classes =  $journal->teachings = $journal->times = '';
            if(!empty($class)) {
                $schedule = Schedule::find($class->schedulesID);
                if(!empty($schedule)){

                    $times = Time::find($schedule->timesID);
                    $teachings = Teaching::find($schedule->teachingsID);
                    $school_classes = SchoolClass::find($schedule->school_classesID);
                    $class_rooms = ClassRoom::find($schedule->class_roomsID);
                    if(!empty($times))
                        $journal->times =  date("H:i",strtotime($times->startLess)).' - '.date("H:i",strtotime($times->endLess));
                    //dd($class->times);
                    if(!empty($teachings)) {
                        $lesson_name = Lesson::find($teachings->lesson);
                        $worker_name = Worker::find($teachings->teacher);
                        $journal->teachings = $lesson_name->name . ' - ' . $worker_name->surname . ' '. $worker_name->name . ' '. $worker_name->patronymic;
                        //dd($worker_name);
                    }
                    if(!empty($school_classes))
                        $journal->school_classes = $school_classes->name;
                    if(!empty($class_rooms))
                        $journal->class_rooms = ' в '.$class_rooms->name.' (ауд)';
                }
                //$journal->classes = $class->class_rooms.' '.$class->teachings;
            }
        }
        //dd($data['journals']);
        return view('journals/main/'.\App\UserRole::getRole(),$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($date = null)
    {
        if(empty($date))
            $date = date("Y-m-d");
        $classes = Classes::getByDate($date);
        $flag = false;
        $classID = 0;
        foreach ($classes as $class) {
            $flag = true;
            $classID = $class->id;
        }
        if($flag == true){
            Journal::create(['classesID'=>$classID]);
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('pk');
        $name = $request->input('name');
        $value = $request->input('value');
        $result = Journal::where('id', '=', $id)->update([$name => $value]);
        $data = [
            'response' => $result
        ];
        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Journal::find($id)->delete();
        return redirect()->back();
    }
}


