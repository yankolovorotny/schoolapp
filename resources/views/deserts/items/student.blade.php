@if ( !empty($deserts) )
    <tr>
        <th>Ученик</th>
        <th>Диамант</th>
        <th>Выдал(а)</th>
        <th>Примечание</th>
    </tr>

    @foreach( $deserts as $desert )

        <tr>
            <td>{{ $desert->student }}</td>
            <td>
                @foreach($diamonds as $diamond)
                    @if($diamond->id == $desert->diamondsID)
                        <img title="{{ $diamond->description }}" height="20" src="{{ $diamond->src }}"/>
                        {{ $diamond->description }}
                    @endif
                @endforeach
            </td>
            <td>{{ $desert->worker }}</td>
            <td>{{ $desert->reason }}</td>
        </tr>
    @endforeach
@endif
<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };

        $('#combodate').editable({
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 2000,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('#combodate').on('hidden', function(e, params) {
            var dates = $('#combodate').text();
            var arr = dates.split('.');
            //alert("/deserts/"+arr[2]+'-'+arr[1]+'-'+arr[0]);
            if(arr[0] == '' || arr[0] == '' || arr[0] == '')
                window.location = "/deserts/";
            else
                window.location = "/deserts/"+arr[2]+'-'+arr[1]+'-'+arr[0];
        });
    }
</script>
