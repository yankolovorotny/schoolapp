@if ( !empty($classrooms) )
    @foreach( $classrooms as $classroom )
        <tr>
            <td><a class="edit-name" data-pk="{{ $classroom->id }}" data-name="name">{{ $classroom->name }}</a></td>
            <td><a class="edit-spec" data-pk="{{ $classroom->id }}" data-name="spec">{{ $classroom->spec }}</a></td>
            <td><a href="{{ route('classrooms/delete',['id' => $classroom->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-name,.edit-spec').editable({
            type: 'text',
            url: '{{ route('classrooms/update') }}',
            title: 'Введите значение'
        });
    }
</script>
