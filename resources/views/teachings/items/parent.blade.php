@if ( !empty($teachings) )
    <tr>
        <th>Преподаватель</th>
        <th>Предмет</th>
    </tr>
    @foreach( $teachings as $teaching )
        <tr>
            <td>{{ $teaching->teacher }}</td>
            <td>{{ $teaching->lesson }}</td>
        </tr>
    @endforeach
@endif
