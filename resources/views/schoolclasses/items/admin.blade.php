@if ( !empty($schoolclasses) )
    @foreach( $schoolclasses as $schoolclass )
        <tr>
            <td><a class="edit-name" data-pk="{{ $schoolclass->id }}" data-name="name">{{ $schoolclass->name }}</a></td>
            <td><a href="{{ route('students') }}#heading{{ $schoolclass->id }}" class="btn btn-xs btn-default pull-right">Список учащихся</a></td>
            <td><a href="{{ route('schoolclasses/delete',['id' => $schoolclass->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-name').editable({
            type: 'text',
            url: '{{ route('schoolclasses/update') }}',
            title: 'Введите значение'
        });
    }
</script>
