@if ( !empty($journals) )
    <tr>
        <th>Занятие</th>
        <th>Ученик</th>
        <th>Оценка</th>
        <th>Тип оценки</th>
        <th>Комментарий</th>
        <th></th>
    </tr>

    @foreach( $journals as $journal )

        <tr>
            <td><a class="edit-classesID" data-pk="{{ $journal->id }}" data-name="classesID" data-type="select" data-source="{{ route('classes/getList',['date'=>$date]) }}">{{$journal->times}} {{$journal->school_classes}} {{$journal->teachings}} {{$journal->class_rooms}}  </a></td>
            <td><a class="edit-studentsID" data-pk="{{ $journal->id }}" data-name="studentsID" data-type="select" data-source="{{ route('students/getList') }}">{{ $journal->student }}</a></td>
            <td><a class="edit-rating" data-pk="{{ $journal->id }}" data-name="rating">{{ $journal->rating }}</a></td>
            <td><a class="edit-rating-type" data-pk="{{ $journal->id }}" data-name="rating_type">{{ $journal->rating_type }}</a></td>
            <td><a class="edit-remark" data-pk="{{ $journal->id }}" data-name="remark">{{ $journal->remark }}</a></td>
            <td><a href="{{ route('journals/delete',['id' => $journal->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-classesID,.edit-studentsID,.edit-rating,.edit-rating-type,.edit-remark').editable({
            url: '{{ route('journals/update') }}',
            title: 'Выберите значение'
        });
        $('#combodate').editable({
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 2000,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('#combodate').on('hidden', function(e, params) {
            var dates = $('#combodate').text();
            var arr = dates.split('.');
            //alert("/journals/"+arr[2]+'-'+arr[1]+'-'+arr[0]);
            if(arr[0] == '' || arr[0] == '' || arr[0] == '')
                window.location = "/journals/";
            else
                window.location = "/journals/"+arr[2]+'-'+arr[1]+'-'+arr[0];
        });
    }
</script>
