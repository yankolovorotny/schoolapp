<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Theme;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'themes' => Theme::orderBy('lesson','ASC')->orderBy('nameoftheme','ASC')->get()
        ];
        foreach ($data['themes'] as $theme){
            $lesson = Lesson::find($theme->lesson);
            if(!empty($lesson))
                $theme->lesson = $lesson->name;
        }
        return view('themes/main/'.\App\UserRole::getRole(),$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $themes = Theme::orderBy('lesson','ASC')->orderBy('nameoftheme','ASC')->get();
        foreach ($themes as $theme){
            $lesson = Lesson::find($theme->lesson);
            if(!empty($lesson))
                $lesson_name = $lesson->name;
            $data[] = ["value" => $theme->id, "text" =>$lesson_name.' - '.$theme->nameoftheme];
        }
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Theme::create(['nameoftheme'=>'Название темы']);
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('pk');
        $name = $request->input('name');
        $value = $request->input('value');
        $result = Theme::where('id', '=', $id)->update([$name => $value]);
        $data = [
            'response' => $result
        ];
        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Theme::find($id)->delete();
        return redirect()->back();
    }
}
