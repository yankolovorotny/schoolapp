@if ( !empty($schoolclasses) )
    @foreach( $schoolclasses as $schoolclass )
        <tr>
            <td>{{ $schoolclass->name }}</td>
            <td><a href="{{ route('students') }}#heading{{ $schoolclass->id }}" class="btn btn-xs btn-default pull-right">Список учащихся</a></td>
        </tr>
    @endforeach
@endif

