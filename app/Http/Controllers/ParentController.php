<?php

namespace App\Http\Controllers;

use App\Parenting;
use App\Student;
use App\StudentParent;
use App\SchoolClass;
use App\UserRole;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\This;

class ParentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'parents' =>  StudentParent::all()
        ];
        foreach ($data['parents'] as $parent){
            $parent->email = '';
            if(User::find($parent->id)){
                $user = User::find($parent->id);
                $parent->email = $user->email;
            }
            $parentings =  Parenting::where('parentID','=',$parent->id)->take(100)->get();
            $childrens = [];
            foreach ($parentings as $parenting){
                $childrens[$parenting->id] = Student::find($parenting->studentID);
            }
            $parent->childrens = $childrens;
            //dd($parent->childrens);
        }
        //dd($data['parents']);
        return view('parents/main/'.\App\UserRole::getRole(),$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        \App\User::create([
            'name'=>'Родитель',
            'role'=>UserRole::where('name','=','parent')->value('id'),
            'password'=>Hash::make('123qweasd'),
            'email'=>'p'.time().'@example.com'
        ]);
        $lastInsertId = \App\User::select('id')->orderBy('id', 'DESC')->first();
        StudentParent::create([
            'id'=>$lastInsertId->id,
            'name'=>'Имя',
            'surname'=>'Фамилия',
            'sex'=>1
        ]);

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('pk');
        $name = $request->input('name');
        $value = $request->input('value');
        if($name == 'child'){
            $result = Parenting::create([
                "studentID"=>$value,
                "parentID"=>$id
            ]);
        }
        elseif ($name == 'child_update'){
            $result = Parenting::where('id','=',$id)->update(['studentID'=>$value]);
        }
        elseif ($name == 'remove_child'){
            $result = Parenting::where('id','=',$id)->delete();
        }
        else
            $result = StudentParent::where('id', '=', $id)->update([$name => $value]);
        $data = [
            'response' => $result
        ];
        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StudentParent::find($id)->delete();
        return redirect()->back();
    }
}
