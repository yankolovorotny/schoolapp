@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Рейтинг оценок
                        <a href="{{ route('statistics') }}">по студентам</a>,&nbsp;
                        <a href="{{ route('statistics/diamonds') }}">по диамантам</a>
                    </div>
                </div>
                @include('statistic.items.index')
            </div>
        </div>
    </div>
@endsection