@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Учителя <a href="{{ route('workers/create') }}" class="btn btn-xs btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Добавить учителя</a></div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                @include('workers.items.'.\App\UserRole::getRole())
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
