## Quick Start
php artisan migrate

php artisan db:seed --class=UsersTableSeeder
php artisan db:seed --class=SchoolClassesTableSeeder
php artisan db:seed --class=UserRolesTableSeeder
php artisan db:seed --class=DayWeeksTableSeeder
php artisan db:seed --class=ClassRoomsTableSeeder
php artisan db:seed --class=DiamondsTableSeeder