@if ( !empty($teachings) )
    <tr>
        <th>Преподаватель</th>
        <th>Предмет</th>
        <th></th>
    </tr>
    @foreach( $teachings as $teaching )
        <tr>
            <td><a class="edit-teacher" data-pk="{{ $teaching->id }}" data-name="teacher" data-type="select" data-source="{{ route('workers/getList') }}">{{ $teaching->teacher }}</a></td>
            <td><a class="edit-lesson" data-pk="{{ $teaching->id }}" data-name="lesson" data-type="select" data-source="{{ route('lessons/getList') }}">{{ $teaching->lesson }}</a></td>
            <td><a href="{{ route('teachings/delete',['id' => $teaching->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-teacher').editable({
            url: '{{ route('teachings/update') }}',
            title: 'Введите значение'
        });
        $('.edit-lesson').editable({
            url: '{{ route('teachings/update') }}',
            title: 'Выберите предмет'
        });
    }
</script>
