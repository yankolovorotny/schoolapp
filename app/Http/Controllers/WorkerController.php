<?php

namespace App\Http\Controllers;

use App\User;
use App\UserRole;
use App\Worker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class WorkerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'workers' =>  Worker::all()
        ];
        foreach ($data['workers'] as $worker){
            $worker->email = '';
            if(User::find($worker->id)){
                $user = User::find($worker->id);
                $worker->email = $user->email;
            }
        }
        //dd($data['workers']);
        return view('workers/main/'.\App\UserRole::getRole(),$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $workers = Worker::all();
        foreach ($workers as $worker){
            $data[] = ["value" => $worker->id, "text" => $worker->surname.' '.$worker->name];
        }
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        \App\User::create([
            'name'=>'Учитель',
            'role'=>UserRole::where('name','=','teacher')->value('id'),
            'password'=>Hash::make('123qweasd'),
            'email'=>'t'.time().'@example.com'
        ]);
        $lastInsertId = \App\User::select('id')->orderBy('id', 'DESC')->first();
        Worker::create([
            'id'=>$lastInsertId->id,
            'name'=>'Имя',
            'surname'=>'Фамилия',
            'sex'=>1
        ]);

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('pk');
        $name = $request->input('name');
        $value = $request->input('value');
            $result = Worker::where('id', '=', $id)->update([$name => $value]);
        $data = [
            'response' => $result
        ];
        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Worker::find($id)->delete();
        return redirect()->back();
    }
}