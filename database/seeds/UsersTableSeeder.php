<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role' => '1',
            'name' => "Admin",
            'email' => "admin@gmail.com",
            'password' => '$2y$10$BG0oeiWcuILmmC81MnyP3.GsVTt3FUxqvgB6x90SbfAZkZ4TAMyJO'
        ]);
    }
}
