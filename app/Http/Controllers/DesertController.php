<?php

namespace App\Http\Controllers;

use App\Classes;
use App\Desert;
use App\Diamond;
use App\SchoolClass;
use App\Student;
use App\Worker;
use Illuminate\Http\Request;

class DesertController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($date = null)
    {
        if(empty($date))
            $date = date("Y-m-d");
        $data = [
            'date' => $date,
            'deserts' => Desert::getByDate($date),
            'diamonds' => Diamond::all()
        ];
        foreach ($data['deserts'] as $desert){
            $desert->classes = $desert->student = $desert->worker = '';
            if(!empty($desert->studentsID)){
                $student = Student::find($desert->studentsID);
                $prop = 'class';
                $schoolclass = SchoolClass::find($student->$prop);
                $desert->student =  $student->surname.' '.$student->name.', '.$schoolclass->name;
            }if(!empty($desert->teacher)){
                $worker = Worker::find($desert->teacher);
                $desert->worker =  $worker->surname.' '.$worker->name.' '.$worker->patronymic;
            }
        }
        //dd($data['deserts']);
        return view('deserts/main/'.\App\UserRole::getRole(),$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param null $date
     * @return \Illuminate\Http\Response
     */
    public function create($date = null)
    {
        if(empty($date))
            $date = date("Y-m-d");
        Desert::create(['date'=>$date]);
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('pk');
        $name = $request->input('name');
        $value = $request->input('value');
        $result = Desert::where('id', '=', $id)->update([$name => $value]);
        $data = [
            'response' => $result
        ];
        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Desert::find($id)->delete();
        return redirect()->back();
    }
}


