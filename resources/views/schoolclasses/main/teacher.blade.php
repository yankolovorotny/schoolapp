@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Школьные классы</div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Название</th>
                                    <th></th>
                                </tr>
                                @include('schoolclasses.items.'.\App\UserRole::getRole())
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
