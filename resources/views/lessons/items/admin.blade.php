@if ( !empty($lessons) )
    @foreach( $lessons as $lesson )
        <tr>
            <td><a class="edit-name" data-pk="{{ $lesson->id }}" data-name="name">{{ $lesson->name }}</a></td>
            <td><a href="{{ route('lessons/delete',['id' => $lesson->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-name').editable({
            type: 'text',
            url: '{{ route('lessons/update') }}',
            title: 'Введите название предмета'
        });
    }
</script>
