@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Темы занятий <a href="{{ route('themes/create') }}" class="btn btn-xs btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Добавить тему</a></div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                @include('themes.items.'.\App\UserRole::getRole())
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
