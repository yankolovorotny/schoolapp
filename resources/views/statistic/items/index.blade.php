@if ( !empty($schoolclasses) )
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach( $schoolclasses as $schoolclass )
            @if ( !empty($journals) )
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading{{$schoolclass->id}}">
                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#collapse{{$schoolclass->id}}" aria-expanded="true"
                           aria-controls="collapse{{$schoolclass->id}}">
                            Учащиеся {{$schoolclass->name}} класса
                        </a>
                    </div>
                    <div id="collapse{{$schoolclass->id}}" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading{{$schoolclass->id}}">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Ученик</th>
                                        <th>Оценка</th>
                                    </tr>

                                    @foreach( $journals as $journal )
                                        @if( $journal->class == $schoolclass->id)
                                            <tr>
                                                <td>{{ $journal->student }}</td>
                                                <td>{{ number_format($journal->rating, 2, '.', '') }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
@endif

<script>
    window.onload = function () {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('#combodate').editable({
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 2000,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('#combodate').on('hidden', function (e, params) {
            var dates = $('#combodate').text();
            var arr = dates.split('.');
            //alert("/journals/"+arr[2]+'-'+arr[1]+'-'+arr[0]);
            if (arr[0] == '' || arr[0] == '' || arr[0] == '')
                window.location = "/journals/";
            else
                window.location = "/journals/" + arr[2] + '-' + arr[1] + '-' + arr[0];
        });
    }
</script>
