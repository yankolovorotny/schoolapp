<?php

use Illuminate\Database\Seeder;

class SchoolClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $postfixes = ["А","Б","В","Г"];
        for($i=1;$i<=11;$i++){
            foreach ($postfixes as $postfix){
                DB::table('school_classes')->insert([
                    'name' => $i.'-'.$postfix
                ]);
            }
        }
    }
}