@if ( !empty($schoolclasses) )
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach( $schoolclasses as $schoolclass )

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{$schoolclass->id}}">
                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#collapse{{$schoolclass->id}}" aria-expanded="true"
                       aria-controls="collapse{{$schoolclass->id}}">
                        Учащиеся {{$schoolclass->name}} класса
                    </a>
                </div>
                <div id="collapse{{$schoolclass->id}}" class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="heading{{$schoolclass->id}}">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>Фамилия</th>
                                    <th>Имя</th>
                                    <th>Отчество</th>
                                    <th>Пол</th>
                                    <th>Дата рождения</th>
                                    <th>Адрес</th>
                                    <th>Телефон</th>
                                </tr>
                                @foreach( $students as $student )
                                    @if( $student->class == $schoolclass->id)
                                            <tr>
                                                <td>
                                                    {{ $student->surname }}
                                                </td>
                                                <td>
                                                    {{ $student->name }}
                                                </td>
                                                <td>
                                                    {{ $student->patronymic }}
                                                </td>
                                                <td>
                                                    {{ (($student->sex == 1)?'М':'Ж') }}
                                                </td>
                                                <td>
                                                    {{ date("d.m.Y",strtotime($student->birthday)) }}
                                                </td>
                                                <td>
                                                    {{ $student->location }}
                                                </td>
                                                <td>
                                                    {{ $student->phone }}
                                                </td>
                                            </tr>
                                            <tr class="info">
                                                <th colspan="7"><span class="glyphicon glyphicon-user"></span>
                                                    {{ $student->email }}
                                                </th>
                                            </tr>
                                    @endif
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
    </div>
@endif

<script>
    window.onload = function () {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
    }
</script>