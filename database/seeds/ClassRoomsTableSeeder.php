<?php

use Illuminate\Database\Seeder;

class ClassRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=60;$i++){
                DB::table('class_rooms')->insert([
                    'name' => $i
                ]);
        }
    }
}
