@if ( !empty($times) )
    @foreach( $times as $time )
        <tr>
            <td><a class="edit-start-less" data-pk="{{ $time->id }}" data-name="startLess" data-type="combodate">{{ date("H:i",strtotime($time->startLess)) }}</a></td>
            <td><a class="edit-end-less" data-pk="{{ $time->id }}" data-name="endLess" data-type="combodate">{{ date("H:i",strtotime($time->endLess)) }}</a></td>
            <td><a href="{{ route('times/delete',['id' => $time->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-start-less,.edit-end-less').editable({
            title: 'Введите время',
            url: '{{ route('times/update') }}',
            format: 'H:mm:ss',
            viewformat: 'H:mm',
            template: 'H:mm',
                    combodate: {
                        minuteStep: 1
                    }
        }
        );
    }
</script>
