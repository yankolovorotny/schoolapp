@if ( !empty($classes) )
    <tr>
        <th>Занятие</th>
        <th>Тема</th>
        <th>Домашнее задание</th>
        <th></th>
    </tr>

    @foreach( $classes as $class )

        <tr>
            <td><a class="edit-schedulesID" data-pk="{{ $class->id }}" data-name="schedulesID" data-type="select" data-source="{{ route('schedules/getList',['date'=>$date]) }}">{{$class->times}} {{$class->school_classes}} {{$class->teachings}} {{$class->class_rooms}} </a></td>
            <td><a class="edit-themesID" data-pk="{{ $class->id }}" data-name="themesID" data-type="select" data-source="{{ route('themes/getList') }}">{{ $class->themes }}</a></td>
            <td><a class="edit-homework" data-pk="{{ $class->id }}" data-name="homework">{{ $class->homework }}</a></td>
            <td><a href="{{ route('classes/delete',['id' => $class->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-schedulesID,.edit-themesID,.edit-homework').editable({
            url: '{{ route('classes/update') }}',
            title: 'Выберите значение'
        });
        $('#combodate').editable({
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 2000,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('#combodate').on('hidden', function(e, params) {
            var dates = $('#combodate').text();
            var arr = dates.split('.');
            //alert("/classes/"+arr[2]+'-'+arr[1]+'-'+arr[0]);
            if(arr[0] == '' || arr[0] == '' || arr[0] == '')
                window.location = "/classes/";
            else
                window.location = "/classes/"+arr[2]+'-'+arr[1]+'-'+arr[0];
        });
    }
</script>
