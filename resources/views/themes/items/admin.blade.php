@if ( !empty($themes) )
    <tr>
        <th>Название</th>
        <th>Предмет</th>
        <th></th>
    </tr>
    @foreach( $themes as $theme )
        <tr>
            <td><a class="edit-name" data-pk="{{ $theme->id }}" data-name="nameoftheme">{{ $theme->nameoftheme }}</a></td>
            <td><a class="edit-lesson" data-pk="{{ $theme->id }}" data-name="lesson" data-type="select" data-source="{{ route('lessons/getList') }}">{{ $theme->lesson }}</a></td>
            <td><a href="{{ route('themes/delete',['id' => $theme->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-name').editable({
            type: 'text',
            url: '{{ route('themes/update') }}',
            title: 'Введите значение'
        });
        $('.edit-lesson').editable({
            url: '{{ route('themes/update') }}',
            title: 'Выберите предмет'
        });
    }
</script>
