<?php

use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            'name' => "admin"
        ]);
        DB::table('user_roles')->insert([
            'name' => "teacher"
        ]);
        DB::table('user_roles')->insert([
            'name' => "student"
        ]);
        DB::table('user_roles')->insert([
            'name' => "parent"
        ]);
    }
}
