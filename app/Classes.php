<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $fillable = ['date'];

    public static function getByDate($date)
    {
        return collect(\DB::select(\DB::raw('SELECT * FROM classes WHERE DATE(date) = "'.$date.'"')));
    }
}
