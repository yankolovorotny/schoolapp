@if ( !empty($schoolclasses) )
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    @foreach( $schoolclasses as $schoolclass )


            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{$schoolclass->id}}">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$schoolclass->id}}" aria-expanded="true" aria-controls="collapse{{$schoolclass->id}}">
                        Учащиеся {{$schoolclass->name}} класса
                    </a>
                </div>
                <div id="collapse{{$schoolclass->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$schoolclass->id}}">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>Фамилия</th>
                                    <th>Имя</th>
                                    <th>Отчество</th>
                                    <th>Пол</th>
                                    <th>Дата рождения</th>
                                    <th>Адрес</th>
                                    <th>Телефон</th>
                                </tr>
                                @foreach( $students as $student )
                                    @if( $student->class == $schoolclass->id)
                                        @if(\Illuminate\Support\Facades\Auth::id() == $student->id )
                                            <tr>
                                                <td>
                                                    <a class="edit-surname" data-pk="{{ $student->id }}" data-name="surname">{{ $student->surname }}</a>
                                                </td>
                                                <td>
                                                    <a class="edit-name" data-pk="{{ $student->id }}" data-name="name">{{ $student->name }}</a>
                                                </td>
                                                <td>
                                                    <a class="edit-patronymic" data-pk="{{ $student->id }}" data-name="patronymic">{{ $student->patronymic }}</a>
                                                </td>
                                                <td>
                                                    <a class="edit-sex" data-pk="{{ $student->id }}" data-name="sex" data-type="select">{{ (($student->sex == 1)?'М':'Ж') }}</a>
                                                </td>
                                                <td>
                                                    <a class="edit-birthday" data-pk="{{ $student->id }}" data-name="birthday" data-type="combodate">{{ date("d.m.Y",strtotime($student->birthday)) }}</a>
                                                </td>
                                                <td>
                                                    <a class="edit-location" data-pk="{{ $student->id }}" data-name="location">{{ $student->location }}</a>
                                                </td>
                                                <td>
                                                    <a class="edit-phone" data-pk="{{ $student->id }}" data-name="phone">{{ $student->phone }}</a>
                                                </td>
                                            </tr>
                                            <tr class="info">
                                                <th colspan="3"><span class="glyphicon glyphicon-user"></span>
                                                    <a class="edit-username" data-pk="{{ $student->id }}" data-name="email">{{ $student->email }}</a>
                                                    <button class="btn btn-xs btn-primary pull-right btn-reset-password" data-pk="{{ $student->id }}">Сброс пароля</button>
                                                </th>
                                                <td colspan="4">Родители:
                                                    @foreach($student->parents as $parent)
                                                        {{$parent->surname}} {{$parent->name}} {{$parent->patronymic}}
                                                    @endforeach
                                                </td>

                                            </tr>
                                        @else
                                            <tr>
                                                <td>
                                                    {{ $student->surname }}
                                                </td>
                                                <td>
                                                    {{ $student->name }}
                                                </td>
                                                <td>
                                                    {{ $student->patronymic }}
                                                </td>
                                                <td>
                                                    {{ (($student->sex == 1)?'М':'Ж') }}
                                                </td>
                                                <td>
                                                    {{ date("d.m.Y",strtotime($student->birthday)) }}
                                                </td>
                                                <td>
                                                    {{ $student->location }}
                                                </td>
                                                <td>
                                                    {{ $student->phone }}
                                                </td>
                                            </tr>
                                            <tr class="info">
                                                <th colspan="3"><span class="glyphicon glyphicon-user"></span>
                                                    {{ $student->email }}
                                                </th>
                                                <td colspan="4">Родители:
                                                    @foreach($student->parents as $parent)
                                                        {{$parent->surname}} {{$parent->name}} {{$parent->patronymic}}
                                                    @endforeach
                                                </td>

                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>

    @endforeach
    </div>
@endif

<script>
    window.onload = function () {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-name,.edit-surname,.edit-patronymic,.edit-location,.edit-phone,.edit-remark').editable({
            type: 'text',
            url: '{{ route('students/update') }}',
            title: 'Введите значение'
        });
        $('.edit-username').editable({
            type: 'text',
            url: '{{ route('users/update') }}',
            title: 'Введите значение'
        });
        $('.edit-sex').editable({
            source: [
                {value: 1, text: 'М'},
                {value: 2, text: 'Ж'}
            ],
            url: '{{ route('students/update') }}',
            title: 'Укажите пол'
        });
        $('.edit-birthday').editable({
            url: '{{ route('students/update') }}',
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 2000,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('body').on('click','.btn-reset-password',function () {
            var pk = $(this).data('pk');
            swal({
                        title: "Пароль",
                        text: "Введите новый пароль",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        animation: "slide-from-top",
                        inputPlaceholder: "Пароль"
                    },
                    function(inputValue){
                        if (inputValue === false) return false;

                        if (inputValue === "") {
                            swal.showInputError("Пароль не может быть пустым!");
                            return false
                        }

                        swal("Пароль изменен!", "Текущий пароль: " + inputValue, "success");

                        $.ajax({
                            type: 'PUT',
                            url: '{{ route('users/update') }}',
                            data: {
                                _token: _token,
                                pk: pk,
                                name: 'password',
                                value: inputValue
                            },
                            success: function (response) {
                            }
                        });

                    });
        })
    }
</script>