@if ( !empty($workers) )
    <tr>
        <th>Фамилия</th>
        <th>Имя</th>
        <th>Отчество</th>
        <th>Пол</th>
        <th>Дата рождения</th>
        <th>Адрес</th>
        <th>Телефон</th>
        <th>Примечание</th>
        <th></th>
    </tr>
    @foreach( $workers as $worker )
        <tr>
            <td>
                <a class="edit-surname" data-pk="{{ $worker->id }}" data-name="surname">{{ $worker->surname }}</a>
            </td>
            <td>
                <a class="edit-name" data-pk="{{ $worker->id }}" data-name="name">{{ $worker->name }}</a>
            </td>
            <td>
                <a class="edit-patronymic" data-pk="{{ $worker->id }}" data-name="patronymic">{{ $worker->patronymic }}</a>
            </td>
            <td>
                <a class="edit-sex" data-pk="{{ $worker->id }}" data-name="sex" data-type="select">{{ (($worker->sex == 1)?'М':'Ж') }}</a>
            </td>
            <td>
                <a class="edit-birthday" data-pk="{{ $worker->id }}" data-name="birthday" data-type="combodate">{{ date("d.m.Y",strtotime($worker->birthday)) }}</a>
            </td>
            <td>
                <a class="edit-location" data-pk="{{ $worker->id }}" data-name="location">{{ $worker->location }}</a>
            </td>
            <td>
                <a class="edit-phone" data-pk="{{ $worker->id }}" data-name="phone">{{ $worker->phone }}</a>
            </td>
            <td>
                <a class="edit-remark" data-pk="{{ $worker->id }}" data-name="remark">{{ $worker->remark }}</a>
            </td>
            <td>
                <a href="{{ route('workers/delete',['id' => $worker->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a>
            </td>
        </tr>
        <tr class="info">
            <th colspan="9"><span class="glyphicon glyphicon-user"></span>
                <a class="edit-username" data-pk="{{ $worker->id }}" data-name="email">{{ $worker->email }}</a>
                <button class="btn btn-xs btn-primary pull-right btn-reset-password" data-pk="{{ $worker->id }}">Сброс пароля</button>
            </th>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-name,.edit-surname,.edit-patronymic,.edit-location,.edit-phone,.edit-remark').editable({
            type: 'text',
            url: '{{ route('workers/update') }}',
            title: 'Введите значение'
        });
        $('.edit-username').editable({
            type: 'text',
            url: '{{ route('users/update') }}',
            title: 'Введите значение'
        });
        $('.edit-sex').editable({
            source: [
                {value: 1, text: 'М'},
                {value: 2, text: 'Ж'}
            ],
            url: '{{ route('workers/update') }}',
            title: 'Укажите пол'
        });
        $('.edit-birthday').editable({
            url: '{{ route('workers/update') }}',
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 1921,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('body').on('click','.btn-reset-password',function () {
            var pk = $(this).data('pk');
            swal({
                        title: "Пароль",
                        text: "Введите новый пароль",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        animation: "slide-from-top",
                        inputPlaceholder: "Пароль"
                    },
                    function(inputValue){
                        if (inputValue === false) return false;

                        if (inputValue === "") {
                            swal.showInputError("Пароль не может быть пустым!");
                            return false
                        }

                        swal("Пароль изменен!", "Текущий пароль: " + inputValue, "success");

                        $.ajax({
                            type: 'PUT',
                            url: '{{ route('users/update') }}',
                            data: {
                                _token: _token,
                                pk: pk,
                                name: 'password',
                                value: inputValue
                            },
                            success: function (response) {
                            }
                        });

                    });
        })
    }
</script>
