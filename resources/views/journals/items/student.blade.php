@if ( !empty($journals) )
    <tr>
        <th>Занятие</th>
        <th>Ученик</th>
        <th>Оценка</th>
        <th>Тип оценки</th>
        <th>Комментарий</th>
    </tr>

    @foreach( $journals as $journal )

        <tr>
            <td>{{$journal->times}} {{$journal->school_classes}} {{$journal->teachings}} {{$journal->class_rooms}}  </td>
            <td>{{ $journal->student }}</td>
            <td>{{ $journal->rating }}</td>
            <td>{{ $journal->rating_type }}</td>
            <td>{{ $journal->remark }}</td>

        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('#combodate').editable({
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 2000,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('#combodate').on('hidden', function(e, params) {
            var dates = $('#combodate').text();
            var arr = dates.split('.');
            //alert("/journals/"+arr[2]+'-'+arr[1]+'-'+arr[0]);
            if(arr[0] == '' || arr[0] == '' || arr[0] == '')
                window.location = "/journals/";
            else
                window.location = "/journals/"+arr[2]+'-'+arr[1]+'-'+arr[0];
        });
    }
</script>
