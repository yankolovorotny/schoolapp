<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::guest())
        return view('auth/login');
    else
        return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'dayweeks'], function() {
    Route::get('/getList/', 'DayWeekController@getList')->name('dayweeks/getList');
});

Route::group(['prefix' => 'schoolclasses'], function() {
    Route::get('/', 'SchoolClassesController@index')->name('schoolclasses');
    Route::get('/delete/{id}', 'SchoolClassesController@destroy')->name('schoolclasses/delete');
    Route::put('/update/', 'SchoolClassesController@update')->name('schoolclasses/update');
    Route::get('/create/', 'SchoolClassesController@create')->name('schoolclasses/create');
    Route::get('/getList/', 'SchoolClassesController@getList')->name('schoolclasses/getList');
});
Route::group(['prefix' => 'students'], function() {
    Route::get('/', 'StudentController@index')->name('students');
    Route::get('/delete/{id}', 'StudentController@destroy')->name('students/delete');
    Route::put('/update/', 'StudentController@update')->name('students/update');
    Route::get('/create/{id}', 'StudentController@create')->name('students/create');
    Route::get('/getList/', 'StudentController@getList')->name('students/getList');
});
Route::group(['prefix' => 'parents'], function() {
    Route::get('/', 'ParentController@index')->name('parents');
    Route::get('/delete/{id}', 'ParentController@destroy')->name('parents/delete');
    Route::put('/update/', 'ParentController@update')->name('parents/update');
    Route::get('/create/', 'ParentController@create')->name('parents/create');
});
Route::group(['prefix' => 'workers'], function() {
    Route::get('/', 'WorkerController@index')->name('workers');
    Route::get('/delete/{id}', 'WorkerController@destroy')->name('workers/delete');
    Route::put('/update/', 'WorkerController@update')->name('workers/update');
    Route::get('/create/', 'WorkerController@create')->name('workers/create');
    Route::get('/getList/', 'WorkerController@getList')->name('workers/getList');
});
Route::group(['prefix' => 'lessons'], function() {
    Route::get('/', 'LessonController@index')->name('lessons');
    Route::get('/delete/{id}', 'LessonController@destroy')->name('lessons/delete');
    Route::put('/update/', 'LessonController@update')->name('lessons/update');
    Route::get('/create/', 'LessonController@create')->name('lessons/create');
    Route::get('/getList/', 'LessonController@getList')->name('lessons/getList');
});
Route::group(['prefix' => 'classrooms'], function() {
    Route::get('/', 'ClassRoomController@index')->name('classrooms');
    Route::get('/delete/{id}', 'ClassRoomController@destroy')->name('classrooms/delete');
    Route::put('/update/', 'ClassRoomController@update')->name('classrooms/update');
    Route::get('/create/', 'ClassRoomController@create')->name('classrooms/create');
    Route::get('/getList/', 'ClassRoomController@getList')->name('classrooms/getList');
});
Route::group(['prefix' => 'times'], function() {
    Route::get('/', 'TimeController@index')->name('times');
    Route::get('/delete/{id}', 'TimeController@destroy')->name('times/delete');
    Route::put('/update/', 'TimeController@update')->name('times/update');
    Route::get('/create/', 'TimeController@create')->name('times/create');
    Route::get('/getList/', 'TimeController@getList')->name('times/getList');
});
Route::group(['prefix' => 'themes'], function() {
    Route::get('/', 'ThemeController@index')->name('themes');
    Route::get('/delete/{id}', 'ThemeController@destroy')->name('themes/delete');
    Route::put('/update/', 'ThemeController@update')->name('themes/update');
    Route::get('/create/', 'ThemeController@create')->name('themes/create');
    Route::get('/getList/', 'ThemeController@getList')->name('themes/getList');
});
Route::group(['prefix' => 'teachings'], function() {
    Route::get('/', 'TeachingController@index')->name('teachings');
    Route::get('/delete/{id}', 'TeachingController@destroy')->name('teachings/delete');
    Route::put('/update/', 'TeachingController@update')->name('teachings/update');
    Route::get('/create/', 'TeachingController@create')->name('teachings/create');
    Route::get('/getList/', 'TeachingController@getList')->name('teachings/getList');
});
Route::group(['prefix' => 'schedules'], function() {
    Route::get('/', 'ScheduleController@index')->name('schedules');
    Route::get('/delete/{id}', 'ScheduleController@destroy')->name('schedules/delete');
    Route::put('/update/', 'ScheduleController@update')->name('schedules/update');
    Route::get('/create/{id}', 'ScheduleController@create')->name('schedules/create');
    Route::get('/getList/{date}', 'ScheduleController@getList')->name('schedules/getList');
});
Route::group(['prefix' => 'classes'], function() {
    Route::get('/{date?}', 'ClassesController@index')->name('classes');
    Route::get('/delete/{id}', 'ClassesController@destroy')->name('classes/delete');
    Route::put('/update/', 'ClassesController@update')->name('classes/update');
    Route::get('/create/{date?}', 'ClassesController@create')->name('classes/create');
    Route::get('/getList/{date}', 'ClassesController@getList')->name('classes/getList');
});
Route::group(['prefix' => 'journals'], function() {
    Route::get('/{date?}', 'JournalController@index')->name('journals');
    Route::get('/delete/{id}', 'JournalController@destroy')->name('journals/delete');
    Route::put('/update/', 'JournalController@update')->name('journals/update');
    Route::get('/create/{date?}', 'JournalController@create')->name('journals/create');
});
Route::group(['prefix' => 'deserts'], function() {
    Route::get('/{date?}', 'DesertController@index')->name('deserts');
    Route::get('/delete/{id}', 'DesertController@destroy')->name('deserts/delete');
    Route::put('/update/', 'DesertController@update')->name('deserts/update');
    Route::get('/create/{date?}', 'DesertController@create')->name('deserts/create');
});
Route::group(['prefix' => 'statistics'], function() {
    Route::get('/', 'StatisticController@index')->name('statistics');
    Route::get('/diamonds/', 'StatisticController@getByDiamonds')->name('statistics/diamonds');
});
Route::group(['prefix' => 'users'], function() {
    //Route::get('/', 'StudentController@index')->name('students');
    //Route::get('/delete/{id}', 'StudentController@destroy')->name('students/delete');
    Route::put('/update/', 'UserController@update')->name('users/update');
    //Route::get('/create/{id}', 'StudentController@create')->name('students/create');
});

