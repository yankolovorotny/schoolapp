<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentParent extends Model
{
    protected $fillable = ['id','name','surname','patronymic','sex'];
}
