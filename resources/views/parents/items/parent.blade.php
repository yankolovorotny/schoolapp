@if ( !empty($parents) )
    <tr>
        <th>Фамилия</th>
        <th>Имя</th>
        <th>Отчество</th>
        <th>Пол</th>
        <th>Дата рождения</th>
        <th>Телефон</th>
    </tr>
    @foreach( $parents as $parent )
        <tr>

            <td>
                {{ $parent->surname }}
            </td>
            <td>
                {{ $parent->name }}
            </td>
            <td>
                {{ $parent->patronymic }}
            </td>
            <td>
                {{ (($parent->sex == 1)?'М':'Ж') }}
            </td>
            <td>
                {{ date("d.m.Y",strtotime($parent->birthday)) }}
            </td>
            <td>
                {{ $parent->phone }}
            </td>
        </tr>
        <tr class="info">
            <th colspan="3"><span class="glyphicon glyphicon-user"></span>
                {{ $parent->email }}
                <a href="mailto:{{ $parent->email }}" class="btn btn-xs btn-primary pull-right "><span class="glyphicon glyphicon-envelope"></span></a>
                @if(!empty($parent->phone))
                    <a href="callto:{{$parent->phone}}" title="{{$parent->phone}}" class="btn btn-xs btn-primary pull-right" style="margin-right:5px"><span class="glyphicon glyphicon-phone"></span></a>&nbsp;
                @endif
            </th>
            <th colspan="3">Дети:
                @foreach( $parent->childrens as $key=>$children )
                    {{$children->surname}} {{$children->name}}
                @endforeach
            </th>
        </tr>
    @endforeach
@endif
