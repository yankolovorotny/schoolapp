<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesertsTable extends Migration
{
    /**
     * Run the migrations. Диаманты, полученные учащимися
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deserts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentsID')->nullable();
            $table->integer('diamondsID')->nullable();
            $table->text('reason')->nullable();
            $table->integer('teacher')->nullable();
            $table->date('date');
            $table->string('src')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deserts');
    }
}
