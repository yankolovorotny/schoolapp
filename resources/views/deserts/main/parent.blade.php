@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Журнал диамантов за
                        <a href="#" id="combodate" data-type="combodate" data-pk="1" data-url="" data-value="{{$date}}" data-title="Выберите дату">{{date("d.m.Y",strtotime($date))}}</a>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                @include('deserts.items.'.\App\UserRole::getRole())
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection