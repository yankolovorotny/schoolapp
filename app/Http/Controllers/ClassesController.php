<?php

namespace App\Http\Controllers;

use App\Classes;
use App\ClassRoom;
use App\DayWeek;
use App\Lesson;
use App\Schedule;
use App\SchoolClass;
use App\Teaching;
use App\Theme;
use App\Time;
use App\UserRole;
use App\Worker;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ClassesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($date = null)
    {
        if(empty($date))
            $date = date("Y-m-d");
        $data = [
            'date' => $date,
            'classes' => Classes::getByDate($date)
        ];
        foreach ($data['classes'] as $class){
                $theme = Theme::find($class->themesID);
            $class->themes = '';
                if(!empty($theme)) {
                    $lesson_name = Lesson::find($theme->lesson);
                    $class->themes = $lesson_name->name . ' - ' . $theme->nameoftheme;
                }
            //
            $schedule = Schedule::find($class->schedulesID);
            $class->class_rooms = $class->school_classes =  $class->teachings = $class->times = '';
            if(!empty($schedule)){

                $times = Time::find($schedule->timesID);
                $teachings = Teaching::find($schedule->teachingsID);
                $school_classes = SchoolClass::find($schedule->school_classesID);
                $class_rooms = ClassRoom::find($schedule->class_roomsID);
                if(!empty($times))
                    $class->times =  date("H:i",strtotime($times->startLess)).' - '.date("H:i",strtotime($times->endLess));
                if(!empty($teachings)) {
                    $lesson_name = Lesson::find($teachings->lesson);
                    $worker_name = Worker::find($teachings->teacher);
                    $class->teachings = $lesson_name->name . ' - ' . $worker_name->surname . ' '. $worker_name->name . ' '. $worker_name->patronymic;//TODO()
                    //dd($worker_name);
                }
                if(!empty($school_classes))
                    $class->school_classes = $school_classes->name;
                if(!empty($class_rooms))
                    $class->class_rooms = ' в '.$class_rooms->name.' (ауд)';
            }
            //


            }
        //dd($data['classes']);
        return view('classes/main/'.\App\UserRole::getRole(),$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList($date = null)
    {
        $data = [
            'date' => $date,
            'classes' => Classes::getByDate($date)
        ];
        foreach ($data['classes'] as $class){
            $theme = Theme::find($class->themesID);
            $class->themes = '';
            if(!empty($theme)) {
                $lesson_name = Lesson::find($theme->lesson);
                $class->themes = $lesson_name->name . ' - ' . $theme->nameoftheme;
            }
            //
            $schedule = Schedule::find($class->schedulesID);
            $class->class_rooms = $class->school_classes =  $class->teachings = $class->times = '';
            
            if(!empty($schedule)){

                $times = Time::find($schedule->timesID);
                $teachings = Teaching::find($schedule->teachingsID);
                $school_classes = SchoolClass::find($schedule->school_classesID);
                $class_rooms = ClassRoom::find($schedule->class_roomsID);
                if(!empty($times))
                    $class->times =  date("H:i",strtotime($times->startLess)).' - '.date("H:i",strtotime($times->endLess));
                if(!empty($teachings)) {
                    $lesson_name = Lesson::find($teachings->lesson);
                    $worker_name = Worker::find($teachings->teacher);
                    $class->teachings = $lesson_name->name . ' - ' . $worker_name->surname . ' '. $worker_name->name . ' '. $worker_name->patronymic;//TODO()
                }
                if(!empty($school_classes))
                    $class->school_classes = $school_classes->name;
                if(!empty($class_rooms))
                    $class->class_rooms = ' в '.$class_rooms->name.' (ауд)';
            }
        }
        foreach ($data['classes'] as $schedule){
            $text = '';
            if(!empty($schedule->times))
                $text .= ' '.$schedule->times;
            if(!empty($schedule->school_classes))
                $text .= ' '.$schedule->school_classes;
            if(!empty($schedule->teachings))
                $text .= ' '.$schedule->teachings;
            if(!empty($schedule->class_rooms))
                $text .= ' в '.$schedule->class_rooms . ' (ауд)';
            $response[] = ["value" => $schedule->id, "text" => $text];
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param null $date
     * @return \Illuminate\Http\Response
     */
    public function create($date = null)
    {
        if(empty($date))
            $date = date("Y-m-d");
        Classes::create(['date'=>$date]);
       return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('pk');
        $name = $request->input('name');
        $value = $request->input('value');
        $result = Classes::where('id', '=', $id)->update([$name => $value]);
        $data = [
            'response' => $result
        ];
        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Classes::find($id)->delete();
        return redirect()->back();
    }
}

