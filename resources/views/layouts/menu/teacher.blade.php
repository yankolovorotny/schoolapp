<li>
    <a href="{{ route('teachings') }}">
        Преподавание
    </a>
</li>
<li>
    <a href="{{ route('classrooms') }}">
        Аудитории
    </a>
</li>
<li>
    <a href="{{ route('lessons') }}">
        Предметы
    </a>
</li>
<li>
    <a href="{{ route('workers') }}">
        Учителя
    </a>
</li>
<li>
    <a href="{{ route('parents') }}">
        Родители
    </a>
</li>
<li>
    <a href="{{ route('students') }}">
        Учащиеся
    </a>
</li>
<li>
    <a href="{{ route('schoolclasses') }}">
        Классы
    </a>
</li>
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
       aria-expanded="false">
        Занятия <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="{{ route('schedules') }}">
                Расписание занятий
            </a>
        </li>
        <li>
            <a href="{{ route('themes') }}">
                Темы занятий
            </a>
        </li>
        <li>
            <a href="{{ route('classes') }}">
                Журнал занятий
            </a>
        </li>
        <li>
            <a href="{{ route('journals') }}">
                Журнал оценок
            </a>
        </li>
        <li>
            <a href="{{ route('deserts') }}">
                Журнал диамантов
            </a>
        </li>
        <li>
            <a href="{{ route('statistics') }}">
                Рейтинг
            </a>
        </li>
    </ul>
</li>