<?php

namespace App\Http\Controllers;

use App\DayWeek;
use Illuminate\Http\Request;

class DayWeekController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $dayweeks = DayWeek::all();
        foreach ($dayweeks as $dayweek){
            $data[] = ["value" => $dayweek->id, "text" =>$dayweek->name];
        }
        return response()->json($data, 200);
    }
}