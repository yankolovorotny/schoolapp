@if ( !empty($workers) )
    <tr>
        <th>Фамилия</th>
        <th>Имя</th>
        <th>Отчество</th>
        <th>Пол</th>
        <th>Дата рождения</th>
    </tr>
    @foreach( $workers as $worker )
        <tr>
            <td>
                {{ $worker->surname }}
            </td>
            <td>
                {{ $worker->name }}
            </td>
            <td>
                {{ $worker->patronymic }}
            </td>
            <td>
                {{ (($worker->sex == 1)?'М':'Ж') }}
            </td>
            <td>
                {{ date("d.m.Y",strtotime($worker->birthday)) }}
            </td>
        </tr>
        <tr class="info">
            <th colspan="5"><span class="glyphicon glyphicon-user"></span>
                {{ $worker->email }}
                <a href="mailto:{{ $worker->email }}" class="btn btn-xs btn-primary pull-right "><span class="glyphicon glyphicon-envelope"></span></a>
            </th>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
    }
</script>
