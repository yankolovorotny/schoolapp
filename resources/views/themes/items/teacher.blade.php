@if ( !empty($themes) )
    <tr>
        <th>Название</th>
        <th>Предмет</th>
    </tr>
    @foreach( $themes as $theme )
        <tr>
            <td>{{ $theme->nameoftheme }}</td>
            <td>{{ $theme->lesson }}</td>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
    }
</script>
