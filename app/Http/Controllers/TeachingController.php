<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Teaching;
use App\Worker;
use Illuminate\Http\Request;

class TeachingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'teachings' => Teaching::orderBy('lesson','ASC')->orderBy('teacher','ASC')->get()
        ];
        foreach ($data['teachings'] as $theme){
            $lesson = Lesson::find($theme->lesson);
            $worker = Worker::find($theme->teacher);
            if(!empty($lesson))
                $theme->lesson = $lesson->name;
            if(!empty($worker))
                $theme->teacher = $worker->surname.' '.$worker->name;
        }
        //dd($data['teachings']);
        return view('teachings/main/'.\App\UserRole::getRole(),$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $teachings = Teaching::all();
        foreach ($teachings as $teaching){
            $lesson = Lesson::find($teaching->lesson);
            $worker = Worker::find($teaching->teacher);
            if(!empty($lesson) && !empty($worker)){
                $data[] = ["value" => $teaching->id, "text" =>$lesson->name.' - '.$worker->surname.' '.$worker->name.' '.$worker->patronymic];
            }
        }
        return response()->json($data, 200);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Teaching::create([]);
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('pk');
        $name = $request->input('name');
        $value = $request->input('value');
        $result = Teaching::where('id', '=', $id)->update([$name => $value]);
        $data = [
            'response' => $result
        ];
        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Teaching::find($id)->delete();
        return redirect()->back();
    }
}
