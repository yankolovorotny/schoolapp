@if ( !empty($dayweeks) )
    @if ( !empty($dayweeks) )
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @foreach( $dayweeks as $dayweek )
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading{{$dayweek->id}}">
                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#collapse{{$dayweek->id}}" aria-expanded="true"
                           aria-controls="collapse{{$dayweek->id}}">
                            {{$dayweek->name}}
                        </a>
                    </div>
                    <div id="collapse{{$dayweek->id}}" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading{{$dayweek->id}}">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Время</th>
                                        <th>День недели</th>
                                        <th>Учитель - предмет</th>
                                        <th>Класс</th>
                                        <th>Кабинет</th>
                                    </tr>
                                    @foreach( $schedules as $schedule )
                                        @if( $schedule->day_weeksID == $dayweek->id)
                                            <tr>
                                                <td>
                                                    {{ $schedule->times }}
                                                </td>
                                                <td>
                                                    {{ $schedule->day_weeks }}
                                                </td>
                                                <td>
                                                    {{ $schedule->teachings }}
                                                </td>
                                                <td>
                                                    {{ $schedule->school_classes }}
                                                </td>
                                                <td>
                                                    {{ $schedule->class_rooms }}
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    @endif
@endif