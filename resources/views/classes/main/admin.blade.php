@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Журнал занятий за
                        <a href="#" id="combodate" data-type="combodate" data-pk="1" data-url="" data-value="{{$date}}" data-title="Выберите дату">{{date("d.m.Y",strtotime($date))}}</a>
                        <a href="{{ route('classes/create',['date'=>$date]) }}" class="btn btn-xs btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Добавить занятие</a></div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                @include('classes.items.'.\App\UserRole::getRole())
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection