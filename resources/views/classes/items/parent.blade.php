@if ( !empty($classes) )
    <tr>
        <th>Занятие</th>
        <th>Тема</th>
        <th>Домашнее задание</th>
    </tr>

    @foreach( $classes as $class )

        <tr>
            <td>{{$class->times}} {{$class->school_classes}} {{$class->teachings}} {{$class->class_rooms}}</td>
            <td>{{ $class->themes }}</td>
            <td>{{ $class->homework }}</td>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('#combodate').editable({
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 2000,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('#combodate').on('hidden', function(e, params) {
            var dates = $('#combodate').text();
            var arr = dates.split('.');
            //alert("/classes/"+arr[2]+'-'+arr[1]+'-'+arr[0]);
            if(arr[0] == '' || arr[0] == '' || arr[0] == '')
                window.location = "/classes/";
            else
                window.location = "/classes/"+arr[2]+'-'+arr[1]+'-'+arr[0];
        });
    }
</script>
