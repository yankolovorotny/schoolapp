@if ( !empty($parents) )
    <tr>
        <th>Фамилия</th>
        <th>Имя</th>
        <th>Отчество</th>
        <th>Пол</th>
        <th>Дата рождения</th>
        <th>Адрес</th>
        <th>Работает в</th>
        <th>Место<br/>работы</th>
        <th>Телефон</th>
        <th>Примечание</th>
        <th></th>
    </tr>
    @foreach( $parents as $parent )
        <tr>

            <td>
                <a class="edit-surname" data-pk="{{ $parent->id }}" data-name="surname">{{ $parent->surname }}</a>
            </td>
            <td>
                <a class="edit-name" data-pk="{{ $parent->id }}" data-name="name">{{ $parent->name }}</a>
            </td>
            <td>
                <a class="edit-patronymic" data-pk="{{ $parent->id }}" data-name="patronymic">{{ $parent->patronymic }}</a>
            </td>
            <td>
                <a class="edit-sex" data-pk="{{ $parent->id }}" data-name="sex" data-type="select">{{ (($parent->sex == 1)?'М':'Ж') }}</a>
            </td>
            <td>
                <a class="edit-birthday" data-pk="{{ $parent->id }}" data-name="birthday" data-type="combodate">{{ date("d.m.Y",strtotime($parent->birthday)) }}</a>
            </td>
            <td>
                <a class="edit-location" data-pk="{{ $parent->id }}" data-name="location">{{ $parent->location }}</a>
            </td>
            <td>
                <a class="edit-profession" data-pk="{{ $parent->id }}" data-name="profession">{{ $parent->profession }}</a>
            </td>
            <td>
                <a class="edit-proflocation" data-pk="{{ $parent->id }}" data-name="proflocation">{{ $parent->proflocation }}</a>
            </td>
            <td>
                <a class="edit-phone" data-pk="{{ $parent->id }}" data-name="phone">{{ $parent->phone }}</a>
            </td>
            <td>
                <a class="edit-remark" data-pk="{{ $parent->id }}" data-name="remark">{{ $parent->remark }}</a>
            </td>
            <td><a href="{{ route('parents/delete',['id' => $parent->id]) }}" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
        <tr class="info">
            <th colspan="3"><span class="glyphicon glyphicon-user"></span>
                <a class="edit-username" data-pk="{{ $parent->id }}" data-name="email">{{ $parent->email }}</a>
                <button class="btn btn-xs btn-primary pull-right btn-reset-password" data-pk="{{ $parent->id }}">Сброс пароля</button>
            </th>
            <th colspan="8">Дети:
                <button class="btn btn-xs btn-primary  btn-append-child" data-pk="{{ $parent->id }}"><span class="glyphicon glyphicon-plus"></span></button>
                @foreach( $parent->childrens as $key=>$children )
                    <span><a class="edit-child" data-pk="{{ $key }}" data-name="child_update" data-type="select" data-source="{{ route('students/getList') }}">{{$children->surname}} {{$children->name}}</a>
                    <button class="btn btn-xs btn-danger  btn-remove-child" data-pk="{{ $key }}"><span class="glyphicon glyphicon-trash"></span></button></span>
                @endforeach
            </th>
        </tr>
    @endforeach
@endif

<script>
    window.onload = function() {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-name,.edit-surname,.edit-patronymic,.edit-location,.edit-profession,.edit-proflocation,.edit-phone,.edit-remark').editable({
            type: 'text',
            url: '{{ route('parents/update') }}',
            title: 'Введите значение'
        });
        $('.edit-username').editable({
            type: 'text',
            url: '{{ route('users/update') }}',
            title: 'Введите значение'
        });
        $('.edit-sex').editable({
            source: [
                {value: 1, text: 'М'},
                {value: 2, text: 'Ж'}
            ],
            url: '{{ route('parents/update') }}',
            title: 'Укажите пол'
        });
        $('.edit-birthday').editable({
            url: '{{ route('parents/update') }}',
            format: 'YYYY-MM-DD',
            viewformat: 'DD.MM.YYYY',
            template: 'D  MMMM  YYYY',
            combodate: {
                minYear: 1921,
                maxYear: {{date("Y")}},
                minuteStep: 1
            }
        });
        $('.edit-child').editable({
            url: '{{ route('parents/update') }}',
            title: 'Выберите ребенка'
        });
        $('body').on('click','.btn-append-child',function () {
            var pk = $(this).data('pk');
            $(this).parent().append(' <span><a class="edit-child" data-pk="'+pk+'" data-name="child" data-type="select" data-source="{{ route('students/getList') }}"></a></span>');
            $('.edit-child').editable({
                url: '{{ route('parents/update') }}',
                title: 'Выберите ребенка'
            });
        });
        $('body').on('click','.btn-remove-child',function () {
            var pk = $(this).data('pk');
            var obj = $(this);
            $.ajax({
                type: 'PUT',
                url: '{{ route('parents/update') }}',
                data: {
                    _token: _token,
                    pk: pk,
                    name: 'remove_child',
                    value: ''
                },
                success: function (response) {
                    $(obj).parent().remove();
                }
            });
        });
        $('body').on('click','.btn-reset-password',function () {
            var pk = $(this).data('pk');
            swal({
                        title: "Пароль",
                        text: "Введите новый пароль",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        animation: "slide-from-top",
                        inputPlaceholder: "Пароль"
                    },
                    function(inputValue){
                        if (inputValue === false) return false;

                        if (inputValue === "") {
                            swal.showInputError("Пароль не может быть пустым!");
                            return false
                        }

                        swal("Пароль изменен!", "Текущий пароль: " + inputValue, "success");

                        $.ajax({
                            type: 'PUT',
                            url: '{{ route('users/update') }}',
                            data: {
                                _token: _token,
                                pk: pk,
                                name: 'password',
                                value: inputValue
                            },
                            success: function (response) {
                            }
                        });

                    });
        })
    }
</script>
