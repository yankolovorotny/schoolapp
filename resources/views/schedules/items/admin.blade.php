@if ( !empty($dayweeks) )
    @if ( !empty($dayweeks) )
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @foreach( $dayweeks as $dayweek )
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading{{$dayweek->id}}">
                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#collapse{{$dayweek->id}}" aria-expanded="true"
                           aria-controls="collapse{{$dayweek->id}}">
                            {{$dayweek->name}}
                        </a>
                        <a role="button" href="{{ route('schedules/create', ['id' => $dayweek->id]) }}"
                           class="btn btn-xs btn-success pull-right"><span class="glyphicon glyphicon-plus"></span>
                            Добавить</a>
                    </div>
                    <div id="collapse{{$dayweek->id}}" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading{{$dayweek->id}}">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Время</th>
                                        <th>День недели</th>
                                        <th>Учитель - предмет</th>
                                        <th>Класс</th>
                                        <th>Кабинет</th>
                                        <th></th>
                                    </tr>
                                    @foreach( $schedules as $schedule )
{{--                                        {{dd($schedule)}}--}}
                                        @if( $schedule->day_weeksID == $dayweek->id)
                                            <tr>
                                                <td>
                                                    <a class="edit-timesID" data-pk="{{ $schedule->id }}"
                                                       data-type="select"
                                                       data-source="{{ route('times/getList') }}"
                                                       data-name="timesID">{{ $schedule->times }}</a>
                                                </td>
                                                <td>
                                                    <a class="edit-day-weeksID" data-pk="{{ $schedule->id }}"
                                                       data-type="select"
                                                       data-source="{{ route('dayweeks/getList') }}"
                                                       data-name="day_weeksID">{{ $schedule->day_weeks }}</a>
                                                </td>
                                                <td>
                                                    <a class="edit-teachings" data-pk="{{ $schedule->id }}"
                                                       data-type="select"
                                                       data-source="{{ route('teachings/getList') }}"
                                                       data-name="teachingsID">{{ $schedule->teachings }}</a>
                                                </td>
                                                <td>
                                                    <a class="edit-school-classes" data-pk="{{ $schedule->id }}"
                                                       data-type="select"
                                                       data-source="{{ route('schoolclasses/getList') }}"
                                                       data-name="school_classesID">{{ $schedule->school_classes }}</a>
                                                </td>
                                                <td>
                                                    <a class="edit-class-rooms" data-pk="{{ $schedule->id }}"
                                                       data-type="select"
                                                       data-source="{{ route('classrooms/getList') }}"
                                                       data-name="class_roomsID">{{ $schedule->class_rooms }}</a>
                                                </td>
                                                <td><a href="{{ route('schedules/delete',['id' => $schedule->id]) }}"
                                                       class="btn btn-xs btn-danger"><span
                                                                class="glyphicon glyphicon-trash"></span></a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    @endif
@endif

<script>
    window.onload = function () {
        var _token = $('input[name="_token"]').val();
        $.fn.editable.defaults.mode = 'modal';
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = $("input[name=_token]").val();
            return params;
        };
        $('.edit-timesID,.edit-day-weeksID,.edit-teachings,.edit-school-classes,.edit-class-rooms').editable({
            url: '{{ route('schedules/update') }}',
            title: 'Выберите предмет'
        });
    }
</script>
