<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserRole extends Model
{
    public static function getRole(){
        $user = User::find(Auth::id());
        return self::where('id','=',$user->role)->value('name');
    }
}
