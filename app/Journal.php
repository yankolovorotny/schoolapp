<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    protected $fillable = ['classesID'];

    public static function getByDate($date)
    {
        return collect(\DB::select(\DB::raw('SELECT * FROM journals a WHERE a.classesID IN (SELECT b.id FROM classes b WHERE date="'.$date.'") ORDER BY a.classesID')));
    }
    public static function getByStudents()
    {
        return collect(\DB::select(\DB::raw('SELECT a.studentsID, b.class, AVG(a.rating) as rating FROM journals a, students b WHERE a.studentsID = b.id  GROUP BY a.studentsID ORDER BY AVG(a.rating) DESC')));
    }
}
