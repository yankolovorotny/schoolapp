<?php

namespace App\Http\Controllers;

use App\ClassRoom;
use App\DayWeek;
use App\Lesson;
use App\Schedule;
use App\SchoolClass;
use App\Teaching;
use App\Time;
use App\Worker;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'dayweeks' => DayWeek::All(),
            'schedules' => Schedule::orderBy('day_weeksID','ASC')->orderBy('timesID','ASC')->get()
        ];
        foreach ($data['schedules'] as $schedule){
            $times = Time::find($schedule->timesID);
            $day_weeks = DayWeek::find($schedule->day_weeksID);
            $teachings = Teaching::find($schedule->teachingsID);
            $school_classes = SchoolClass::find($schedule->school_classesID);
            $class_rooms = ClassRoom::find($schedule->class_roomsID);
            if(!empty($times))
                $schedule->times =  date("H:i",strtotime($times->startLess)).' - '.date("H:i",strtotime($times->endLess));
            if(!empty($day_weeks))
                $schedule->day_weeks = $day_weeks->name;
            if(!empty($teachings)) {
                $lesson_name = Lesson::find($teachings->lesson);
                $worker_name = Worker::find($teachings->teacher);
                $schedule->teachings = $lesson_name->name . ' - ' . $worker_name->surname . ' '. $worker_name->name . ' '. $worker_name->patronymic;//TODO()
                //dd($worker_name);
            }
            if(!empty($school_classes))
                $schedule->school_classes = $school_classes->name;
            if(!empty($class_rooms))
                $schedule->class_rooms = $class_rooms->name;
        }
        //dd($data['schedules']);
        return view('schedules/main/'.\App\UserRole::getRole(),$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList($date = null)
    {
        $data = [];
        if(empty($date))
            $date = date('Y-m-d');
        $a = $date;
        $response = [
            'schedules' => Schedule::where('day_weeksID','=',date('w',strtotime($a)))->orderBy('day_weeksID','ASC')->orderBy('timesID','ASC')->get()
        ];
        foreach ($response['schedules'] as $schedule){
            $times = Time::find($schedule->timesID);
            $day_weeks = DayWeek::find($schedule->day_weeksID);
            $teachings = Teaching::find($schedule->teachingsID);
            $school_classes = SchoolClass::find($schedule->school_classesID);
            $class_rooms = ClassRoom::find($schedule->class_roomsID);
            if(!empty($times))
                $schedule->times =  date("H:i",strtotime($times->startLess)).' - '.date("H:i",strtotime($times->endLess));
            if(!empty($day_weeks))
                $schedule->day_weeks = $day_weeks->name;
            if(!empty($teachings)) {
                $lesson_name = Lesson::find($teachings->lesson);
                $worker_name = Worker::find($teachings->teacher);
                $schedule->teachings = $lesson_name->name . ' - ' . $worker_name->surname . ' '. $worker_name->name . ' '. $worker_name->patronymic;//TODO()
                //dd($worker_name);
            }
            if(!empty($school_classes))
                $schedule->school_classes = $school_classes->name;
            if(!empty($class_rooms))
                $schedule->class_rooms = $class_rooms->name;
        }
        foreach ($response['schedules'] as $schedule){
            $text = '';
            if(!empty($schedule->times))
                $text .= ' '.$schedule->times;
            if(!empty($schedule->school_classes))
                $text .= ' '.$schedule->school_classes;
                if(!empty($schedule->teachings))
                    $text .= ' '.$schedule->teachings;
            if(!empty($schedule->class_rooms))
                $text .= ' в '.$schedule->class_rooms . ' (ауд)';
                $data[] = ["value" => $schedule->id, "text" => $text];
        }
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        Schedule::create(['day_weeksID'=>$id]);
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('pk');
        $name = $request->input('name');
        $value = $request->input('value');
        $result = Schedule::where('id', '=', $id)->update([$name => $value]);
        $data = [
            'response' => $result
        ];
        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Schedule::find($id)->delete();
        return redirect()->back();
    }
}
